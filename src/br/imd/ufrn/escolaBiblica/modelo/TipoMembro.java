package br.imd.ufrn.escolaBiblica.modelo;

public enum TipoMembro {
	ALUNO,VISITANTE,PROFESSOR,SUPERINTENDENTE
}
