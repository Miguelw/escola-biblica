package br.imd.ufrn.escolaBiblica.modelo;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import sun.security.jca.GetInstance;

@Entity
public class Membro {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "matricula")
	private long matricula;

	@Column(name = "nome")
	private String nome;

	@Temporal(TemporalType.DATE)
	@Column(name = "data_nascimento")
	private Date dataNascimento;

	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_membro_enum")
	private TipoMembro tipoMembro;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "membro")
	@Column(name = "id_frequencia")
	private List<Frequencia> frequencias;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "membro")
	@Column(name = "id_participacao")
	private List<Participacao> participacoes;

	public Membro() {
	}

	public Membro(String nome, Date dataNascimento, TipoMembro tipoMembro, List<Frequencia> frequencias,
			List<Participacao> participacoes) {
		this.nome = nome;
		this.dataNascimento = dataNascimento;
		this.tipoMembro = tipoMembro;
		this.frequencias = frequencias;
		this.participacoes = participacoes;
	}

	public long getMatricula() {
		return matricula;
	}

	public void setMatricula(long matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public TipoMembro getTipoMembro() {
		return tipoMembro;
	}

	public void setTipoMembro(TipoMembro tipoMembro) {
		this.tipoMembro = tipoMembro;
	}

	public List<Frequencia> getFrequencias() {
		return frequencias;
	}

	public void setFrequencias(List<Frequencia> frequencias) {
		this.frequencias = frequencias;
	}

	public List<Participacao> getParticipacoes() {
		return participacoes;
	}

	public void setParticipacoes(List<Participacao> participacoes) {
		this.participacoes = participacoes;
	}
	
	public int getIdade(){
		
		int idade;
		
		Calendar dataAtual = Calendar.getInstance();
		Calendar dataNascimento = Calendar.getInstance();
		Calendar dataComparacao = Calendar.getInstance();
		
		dataNascimento.setTime(this.dataNascimento);
		dataComparacao.setTime(this.dataNascimento);
		dataComparacao.set(Calendar.YEAR, dataAtual.YEAR);
		
		idade = dataAtual.get(Calendar.YEAR) - dataNascimento.get(Calendar.YEAR);
		
		if (dataComparacao.compareTo(dataAtual) == -1){
			idade--;
			return idade;
		}else{
			return idade;
		}
	}

}
