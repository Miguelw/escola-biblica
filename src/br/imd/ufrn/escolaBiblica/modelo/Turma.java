package br.imd.ufrn.escolaBiblica.modelo;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Turma {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String nome;

	private Integer idadeMinima;

	private Integer idadeMaxima;

	@OneToOne
	private Membro professor;

	@OneToMany
	private List<Membro> alunos;

	private BigDecimal oferta;

	public Turma() {		
	}

	public Turma(Membro professor, List<Membro> alunos) {
		this.professor = professor;
		this.alunos = alunos;
	}

	public Membro getProfessor() {
		return professor;
	}

	public void setProfessor(Membro professor) {
		this.professor = professor;
	}

	public List<Membro> getAlunos() {
		return alunos;
	}

	public void setAlunos(List<Membro> alunos) {
		this.alunos = alunos;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public BigDecimal getOferta() {
		return oferta;
	}

	public void setOferta(BigDecimal oferta) {
		this.oferta = oferta;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getIdadeMinima() {
		return idadeMinima;
	}

	public void setIdadeMinima(Integer idadeMinima) {
		this.idadeMinima = idadeMinima;
	}

	public Integer getIdadeMaxima() {
		return idadeMaxima;
	}

	public void setIdadeMaxima(Integer idadeMaxima) {
		this.idadeMaxima = idadeMaxima;
	}

}
