package br.imd.ufrn.escolaBiblica.modelo;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Coordenacao {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;

	@OneToMany
	@Column(name = "id_turmas")
	private List<Turma> turmas;

	@OneToOne	
	private Membro superintendente;

	public Coordenacao() {
	}

	public Coordenacao(List<Turma> turmas, Membro superintendente) {
		this.turmas = turmas;
		this.superintendente = superintendente;
	}

	public List<Turma> getTurmas() {
		return turmas;
	}

	public void setTurmas(List<Turma> turmas) {
		this.turmas = turmas;
	}

	public Membro getSuperintendente() {
		return superintendente;
	}

	public void setSuperintendente(Membro superintendente) {
		this.superintendente = superintendente;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

}
