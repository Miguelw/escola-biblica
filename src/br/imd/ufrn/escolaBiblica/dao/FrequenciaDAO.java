package br.imd.ufrn.escolaBiblica.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import br.imd.ufrn.escolaBiblica.dao.GenericDAO;
import br.imd.ufrn.escolaBiblica.modelo.Frequencia;
import br.imd.ufrn.escolaBiblica.modelo.Membro;
import br.imd.ufrn.escolaBiblica.util.JPAUtil;


public class FrequenciaDAO implements GenericDAO{

	@PersistenceContext(unitName = "escolabiblicalocal")
	private EntityManager manager = new JPAUtil().getEntityManager();

	@Override
	public void inserir(Object frequencia) {
		manager.getTransaction().begin();

		manager.persist(frequencia);

		manager.getTransaction().commit();
	}

	@Override
	public void remover(Object frequencia) {
		manager.getTransaction().begin();

		Frequencia frequenciaRemovido = (Frequencia) buscar(frequencia);
		manager.remove(manager.contains(frequenciaRemovido) ? frequenciaRemovido : manager.merge(frequenciaRemovido));

		manager.getTransaction().commit();
	}

	@Override
	public void atualizar(Object frequencia) {
		manager.getTransaction().begin();
		manager.merge(frequencia);
		manager.getTransaction().commit();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object> listar() {
		return manager.createQuery("select f from Frequencia f").getResultList();
	}

	@Override
	public Object buscar(Object frequencia) {
		Frequencia query = manager.find(Frequencia.class, ((Frequencia) frequencia).getId());
		return query;
	}
	
	public Object buscar(Membro membro, Date dataFrequencia) {
		
		try {
			String hql = "select f from Frequencia f where f.membro = :membro" +
				    " and f.data = :data";
			
			return manager.createQuery(hql)
					.setParameter("membro", membro)
					.setParameter("data", dataFrequencia).getSingleResult();
			
		} catch (NoResultException e) {
			return null;
		}
		
	}
	
}
