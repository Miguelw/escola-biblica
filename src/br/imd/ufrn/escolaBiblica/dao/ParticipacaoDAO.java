package br.imd.ufrn.escolaBiblica.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import br.imd.ufrn.escolaBiblica.dao.GenericDAO;
import br.imd.ufrn.escolaBiblica.modelo.Participacao;
import br.imd.ufrn.escolaBiblica.modelo.Membro;
import br.imd.ufrn.escolaBiblica.util.JPAUtil;


public class ParticipacaoDAO implements GenericDAO{

	@PersistenceContext(unitName = "escolabiblicalocal")
	private EntityManager manager = new JPAUtil().getEntityManager();

	@Override
	public void inserir(Object participacao) {
		manager.getTransaction().begin();

		manager.persist(participacao);

		manager.getTransaction().commit();
	}

	@Override
	public void remover(Object participacao) {
		manager.getTransaction().begin();

		Participacao participacaoRemovido = (Participacao) buscar(participacao);
		manager.remove(manager.contains(participacaoRemovido) ? participacaoRemovido : manager.merge(participacaoRemovido));

		manager.getTransaction().commit();
	}

	@Override
	public void atualizar(Object participacao) {
		manager.getTransaction().begin();
		manager.merge(participacao);
		manager.getTransaction().commit();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object> listar() {
		return manager.createQuery("select f from Participacao f").getResultList();
	}

	@Override
	public Object buscar(Object participacao) {
		Participacao query = manager.find(Participacao.class, ((Participacao) participacao).getId());
		return query;
	}
	
	public Object buscar(Membro membro, Date dataParticipacao) {
		
		try {
			String hql = "select f from Participacao f where f.membro = :membro" +
				    " and f.data = :data";
			
			return manager.createQuery(hql)
					.setParameter("membro", membro)
					.setParameter("data", dataParticipacao).getSingleResult();
			
		} catch (NoResultException e) {
			return null;
		}
		
	}
	
}
