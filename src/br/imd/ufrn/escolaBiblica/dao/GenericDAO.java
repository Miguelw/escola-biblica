package br.imd.ufrn.escolaBiblica.dao;

import java.util.List;

public interface GenericDAO {

	public void inserir(Object o);

	public void remover(Object o);

	public void atualizar(Object o);

	public List<Object> listar();
	
	public Object buscar(Object membro);
}
