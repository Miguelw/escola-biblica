package br.imd.ufrn.escolaBiblica.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;

import br.imd.ufrn.escolaBiblica.modelo.Turma;
import br.imd.ufrn.escolaBiblica.util.JPAUtil;

public class TurmaDAO implements GenericDAO {

	@PersistenceContext(unitName = "escolabiblicalocal")
	private EntityManager manager = new JPAUtil().getEntityManager();

	@Override
	public void inserir(Object turma) {
		manager.getTransaction().begin();

		manager.persist(turma);

		manager.getTransaction().commit();
	}

	@Override
	public void remover(Object turma) {
		manager.getTransaction().begin();

		Turma TurmaRemovida = (Turma) buscar(turma);
		manager.remove(manager.contains(TurmaRemovida) ? TurmaRemovida : manager.merge(TurmaRemovida));

		manager.getTransaction().commit();
	}

	@Override
	public void atualizar(Object turma) {

		manager.getTransaction().begin();
		manager.merge(turma);
		manager.getTransaction().commit();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object> listar() {
		return manager.createQuery("select t from Turma t").getResultList();
	}

	@Override
	public Object buscar(Object membro) {
		Turma query = manager.find(Turma.class, ((Turma) membro).getId());
		return query;
	}

}
