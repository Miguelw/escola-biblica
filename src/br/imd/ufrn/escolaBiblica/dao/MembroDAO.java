package br.imd.ufrn.escolaBiblica.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.imd.ufrn.escolaBiblica.modelo.Membro;
import br.imd.ufrn.escolaBiblica.util.JPAUtil;

public class MembroDAO implements GenericDAO {

	@PersistenceContext(unitName = "escolabiblicalocal")
	private EntityManager manager = new JPAUtil().getEntityManager();

	@Override
	public void inserir(Object membro) {
		manager.getTransaction().begin();

		manager.persist(membro);

		manager.getTransaction().commit();
	}

	@Override
	public void remover(Object membro) {
		manager.getTransaction().begin();

		Membro membroRemovido = (Membro) buscar(membro);
		manager.remove(manager.contains(membroRemovido) ? membroRemovido : manager.merge(membroRemovido));

		manager.getTransaction().commit();
	}

	@Override
	public void atualizar(Object membro) {
		manager.getTransaction().begin();
		manager.merge(membro);
		manager.getTransaction().commit();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object> listar() {
		return manager.createQuery("select m from Membro m").getResultList();
	}

	@Override
	public Object buscar(Object membro) {
		Membro query = manager.find(Membro.class, ((Membro) membro).getMatricula());
		return query;
	}
}
