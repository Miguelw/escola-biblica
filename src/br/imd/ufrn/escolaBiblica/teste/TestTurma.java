package br.imd.ufrn.escolaBiblica.teste;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import br.imd.ufrn.escolaBiblica.dao.GenericDAO;
import br.imd.ufrn.escolaBiblica.dao.MembroDAO;
import br.imd.ufrn.escolaBiblica.dao.TurmaDAO;
import br.imd.ufrn.escolaBiblica.modelo.Turma;
import br.imd.ufrn.escolaBiblica.modelo.Membro;

public class TestTurma {

	public static void populaTurma() {

		GenericDAO alunoDAO = new MembroDAO();
		GenericDAO dao = new TurmaDAO();

		Turma turma = new Turma();
		List<Membro> membros = new ArrayList<>();
		turma.setAlunos(membros);
		turma.setOferta(new BigDecimal(15.05));

		Object membro = new Membro();
		((Membro) membro).setMatricula(1);

		turma.setProfessor((Membro) alunoDAO.buscar(membro));
		dao.inserir(turma);
	}

	public static void adicionaAlunosEmTurma(long idTurma) {
		GenericDAO alunosDAO = new MembroDAO();
		GenericDAO turmaDAO = new TurmaDAO();

		List<Membro> membros = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			membros.add(new Membro());
			membros.get(i).setNome("Aluno" + i);
			alunosDAO.inserir(membros.get(i));
		}

		Turma turma = new Turma();
		turma.setAlunos(membros);
		turma.setId(idTurma);

		turmaDAO.atualizar(turma);

	}

	public static long criaTurma() {
		Turma turma = new Turma();
		GenericDAO turmaDAO = new TurmaDAO();

		turmaDAO.inserir(turma);
		return turma.getId();
	}

	public static void main(String[] args) {
		// populaTurma();
		long idTurma = criaTurma();
		adicionaAlunosEmTurma(idTurma);

	}

}
