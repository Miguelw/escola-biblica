package br.imd.ufrn.escolaBiblica.teste;

import java.util.Date;
import java.util.List;

import br.imd.ufrn.escolaBiblica.dao.MembroDAO;
import br.imd.ufrn.escolaBiblica.modelo.Membro;
import br.imd.ufrn.escolaBiblica.modelo.TipoMembro;

public class TestMembro {

	public static void popula() {
		MembroDAO dao = new MembroDAO();

		Membro membro = new Membro();

		membro.setNome("miguel");
		membro.setTipoMembro(TipoMembro.PROFESSOR);
		dao.inserir(membro);

	}

	public static void remover() {
		MembroDAO dao = new MembroDAO();

		Membro membro = new Membro();
		membro.setMatricula(1);
		dao.remover(membro);
	}

	public static void atualizar() {
		MembroDAO dao = new MembroDAO();

		Membro membro = new Membro();
		membro.setMatricula(2);

		membro = (Membro) dao.buscar(membro);

		membro.setNome("miguel");
		membro.setTipoMembro(TipoMembro.ALUNO);
		membro.setDataNascimento(new Date());
		dao.atualizar(membro);

	}

	public static void listaMembros() {
		MembroDAO dao = new MembroDAO();
		List<Object> membros = dao.listar();
		for (Object membro : membros) {
			System.out.println(((Membro) membro).getNome() + " " + ((Membro) membro).getMatricula());
		}
	}

	public static void buscaMembro() {
		MembroDAO dao = new MembroDAO();
		Membro membro = new Membro();

		membro.setMatricula(1);
		Membro query = (Membro) dao.buscar(membro);

		System.out.println("Nome: " + query.getNome() + "\nMatricula: " + query.getMatricula());
	}

	public static void main(String[] args) {
		// buscaMembro();
		popula();
		listaMembros();
	}

}
