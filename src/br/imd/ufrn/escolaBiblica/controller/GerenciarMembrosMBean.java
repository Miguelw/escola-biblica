package br.imd.ufrn.escolaBiblica.controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.imd.ufrn.escolaBiblica.dao.GenericDAO;
import br.imd.ufrn.escolaBiblica.dao.MembroDAO;
import br.imd.ufrn.escolaBiblica.modelo.Membro;
import br.imd.ufrn.escolaBiblica.modelo.TipoMembro;

@ManagedBean
@SessionScoped
public class GerenciarMembrosMBean {

	private Membro membro;
	private List<Membro> membros = new ArrayList<Membro>();
	private TipoMembro[] tiposMembro;
	private String filtroNome;
	private String filtroMatricula;
	private boolean cadastrado;

	private GenericDAO dao = new MembroDAO();

	public GerenciarMembrosMBean() {
		this.setTiposMembro(TipoMembro.values());
		this.membros = (List<Membro>) (Object) dao.listar();
		this.setCadastrado(false);
	}

	public String prepararNovoMembro() {
		this.cadastrado = false;
		this.membro = new Membro();
		return "cadastrarNovoMembro";
	}

	public String cadastrar() {
		dao.inserir(this.membro);
		this.membros = (List<Membro>) (Object) dao.listar();
		return "gerenciarMembros";
	}

	public String prepararAlteracao() {
		this.setCadastrado(true);
		return "cadastrarNovoMembro";
	}

	public String alterar() {
		dao.atualizar(membro);
		this.membros = (List<Membro>) (Object) dao.listar();
		return "gerenciarMembros";
	}

	public String remover() {
		dao.remover(membro);
		this.membros = (List<Membro>) (Object) dao.listar();
		return "gerenciarMembros";
	}
	
	public String buscar() {
		
		this.membros = (List<Membro>) (Object) dao.listar();
		List<Membro> resultadoBusca = new ArrayList<Membro>();
		
		for(Membro membroBusca : this.membros){
			if (membroBusca.getNome().contains(this.filtroNome)){
				resultadoBusca.add(membroBusca);
			}
		}
		
		this.membros.clear();
		this.setMembros(resultadoBusca);
		return "gerenciarMembros";
	}

	public List<Membro> getMembros() {
		return membros;
	}

	public void setMembros(List<Membro> membros) {
		this.membros = membros;
	}

	public Membro getMembro() {
		this.membros = (List<Membro>) (Object) dao.listar();
		return membro;
	}

	public void setMembro(Membro membro) {
		this.membro = membro;
	}

	public TipoMembro[] getTiposMembro() {
		return tiposMembro;
	}

	public void setTiposMembro(TipoMembro[] tiposMembro) {
		this.tiposMembro = tiposMembro;
	}

	public String getFiltroNome() {
		return filtroNome;
	}

	public void setFiltroNome(String filtroNome) {
		this.filtroNome = filtroNome;
	}

	public String getFiltroMatricula() {
		return filtroMatricula;
	}

	public void setFiltroMatricula(String filtroMatricula) {
		this.filtroMatricula = filtroMatricula;
	}

	public boolean isCadastrado() {
		return cadastrado;
	}

	public void setCadastrado(boolean cadastrado) {
		this.cadastrado = cadastrado;
	}

}
