package br.imd.ufrn.escolaBiblica.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import br.imd.ufrn.escolaBiblica.modelo.Membro;
import br.imd.ufrn.escolaBiblica.modelo.Turma;
import br.imd.ufrn.escolaBiblica.dao.FrequenciaDAO;
import br.imd.ufrn.escolaBiblica.dao.MembroDAO;
import br.imd.ufrn.escolaBiblica.dao.TurmaDAO;
import br.imd.ufrn.escolaBiblica.modelo.Frequencia;

@ManagedBean
@SessionScoped
public class MarcarFrequenciaMBean {
	
	private Date dataDaFrequencia;
	private Frequencia frequencia;
	private List<Membro> membros;
	private List<Frequencia> frequenciasDoDia;
	private FrequenciaDAO frequenciaDAO;
	private ArrayList<Turma> turmas;
	private Turma turma;

	public MarcarFrequenciaMBean() {
		
		TurmaDAO turmaDAO = new TurmaDAO();
		frequenciaDAO = new FrequenciaDAO();
		this.frequenciasDoDia = new ArrayList<Frequencia>();
		this.turmas = (ArrayList<Turma>) (Object) turmaDAO.listar();
	}
	
	public String selecionarTurma(){
		return "marcarFrequencia";
	}
	
	public void carregarFrequencias(){
		
		if (this.turma != null){
			
			this.membros = this.turma.getAlunos();
			this.frequenciasDoDia.clear();
			Frequencia frequencia;
			
			for (Membro aluno : this.membros){
				if(frequenciaDAO.buscar(aluno, this.dataDaFrequencia) != null){
					this.frequenciasDoDia.add((Frequencia) frequenciaDAO.buscar(aluno, this.dataDaFrequencia));
				}else{
					frequencia = new Frequencia();
					frequencia.setData(dataDaFrequencia);
					frequencia.setPresenca(false);
					frequencia.setMembro(aluno);
					this.frequenciasDoDia.add(frequencia);
				}
			}
		}
	}
	
	public String salvarFrequencias(){
		for (Frequencia frequencia : this.frequenciasDoDia){
			frequenciaDAO.inserir(frequencia);
		}
		
		return "marcarFrequencia";
	}
	
	public Date getDataDaFrequencia() {
		return dataDaFrequencia;
	}

	public void setDataDaFrequencia(Date dataDaFrequencia) {
		this.dataDaFrequencia = dataDaFrequencia;
	}

	public Frequencia getFrequencia() {
		return frequencia;
	}

	public void setFrequencia(Frequencia frequencia) {
		this.frequencia = frequencia;
	}

	public List<Membro> getMembros() {
		return membros;
	}

	public void setMembros(List<Membro> membros) {
		this.membros = membros;
	}

	public List<Frequencia> getFrequenciasDoDia() {
		return frequenciasDoDia;
	}

	public void setFrequenciasDoDia(List<Frequencia> frequenciasDoDia) {
		this.frequenciasDoDia = frequenciasDoDia;
	}

	public ArrayList<Turma> getTurmas() {
		return turmas;
	}

	public void setTurmas(ArrayList<Turma> turmas) {
		this.turmas = turmas;
	}

	public Turma getTurma() {
		return turma;
	}

	public void setTurma(Turma turma) {
		this.turma = turma;
	}
	
}
