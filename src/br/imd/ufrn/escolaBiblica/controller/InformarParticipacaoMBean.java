package br.imd.ufrn.escolaBiblica.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import br.imd.ufrn.escolaBiblica.modelo.Membro;
import br.imd.ufrn.escolaBiblica.modelo.Turma;
import br.imd.ufrn.escolaBiblica.dao.ParticipacaoDAO;
import br.imd.ufrn.escolaBiblica.dao.MembroDAO;
import br.imd.ufrn.escolaBiblica.dao.TurmaDAO;
import br.imd.ufrn.escolaBiblica.modelo.Participacao;

@ManagedBean
@SessionScoped
public class InformarParticipacaoMBean {
	
	private Date dataDaParticipacao;
	private Participacao participacao;
	private List<Membro> membros;
	private List<Participacao> participacoesDoDia;
	private ParticipacaoDAO participacaoDAO;
	private ArrayList<Turma> turmas;
	private Turma turma;
	private Double nota;

	public InformarParticipacaoMBean() {
		
		TurmaDAO turmaDAO = new TurmaDAO();
		participacaoDAO = new ParticipacaoDAO();
		this.participacoesDoDia = new ArrayList<Participacao>();
		this.turmas = (ArrayList<Turma>) (Object) turmaDAO.listar();
	}
	
	public String selecionarTurma(){
		return "informarParticipacao";
	}
	
	public void carregarParticipacoes(){
		
		if (this.turma != null){
			
			this.membros = this.turma.getAlunos();
			this.participacoesDoDia.clear();
			Participacao participacao;
			
			for (Membro aluno : this.membros){
				if(participacaoDAO.buscar(aluno, this.dataDaParticipacao) != null){
					this.participacoesDoDia.add((Participacao) participacaoDAO.buscar(aluno, this.dataDaParticipacao));
				}else{
					participacao = new Participacao();
					participacao.setData(dataDaParticipacao);
					participacao.setMembro(aluno);
					this.participacoesDoDia.add(participacao);
				}
			}
		}
	}
	
	public String salvarParticipacoes(){
		for (Participacao participacao : this.participacoesDoDia){
			participacaoDAO.inserir(participacao);
		}
		
		return "marcarParticipacao";
	}
	
	public Date getDataDaParticipacao() {
		return dataDaParticipacao;
	}

	public void setDataDaParticipacao(Date dataDaParticipacao) {
		this.dataDaParticipacao = dataDaParticipacao;
	}

	public Participacao getParticipacao() {
		return participacao;
	}

	public void setParticipacao(Participacao participacao) {
		this.participacao = participacao;
	}

	public List<Membro> getMembros() {
		return membros;
	}

	public void setMembros(List<Membro> membros) {
		this.membros = membros;
	}

	public List<Participacao> getParticipacoesDoDia() {
		return participacoesDoDia;
	}

	public void setParticipacoesDoDia(List<Participacao> participacoesDoDia) {
		this.participacoesDoDia = participacoesDoDia;
	}

	public ArrayList<Turma> getTurmas() {
		return turmas;
	}

	public void setTurmas(ArrayList<Turma> turmas) {
		this.turmas = turmas;
	}

	public Turma getTurma() {
		return turma;
	}

	public void setTurma(Turma turma) {
		this.turma = turma;
	}

	public Double getNota() {
		return nota;
	}

	public void setNota(Double nota) {
		this.nota = nota;
	}
	
}
