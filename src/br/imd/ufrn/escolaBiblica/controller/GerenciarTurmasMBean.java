package br.imd.ufrn.escolaBiblica.controller;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;

import br.imd.ufrn.escolaBiblica.dao.GenericDAO;
import br.imd.ufrn.escolaBiblica.dao.MembroDAO;
import br.imd.ufrn.escolaBiblica.dao.TurmaDAO;
import br.imd.ufrn.escolaBiblica.modelo.Membro;
import br.imd.ufrn.escolaBiblica.modelo.TipoMembro;
import br.imd.ufrn.escolaBiblica.modelo.Turma;

@ManagedBean
@SessionScoped
public class GerenciarTurmasMBean {

	private Turma turma;
	private List<Turma> turmas = new ArrayList<Turma>();
	private BigDecimal oferta;
	private boolean cadastrada;

	private GenericDAO dao = new TurmaDAO();

	public GerenciarTurmasMBean() {
		this.turmas = (List<Turma>) (Object) dao.listar();
		this.setCadastrada(false);
	}

	public String prepararNovaTurma() {
		this.cadastrada = false;
		this.turma = new Turma();
		return "turma";
	}

	public String cadastrar() {
		dao.inserir(this.getTurma());
		this.turmas = (List<Turma>) (Object) dao.listar();
		return "gerenciarTurmas";
	}

	public String prepararAlteracao() {
		this.setCadastrada(true);
		return "turma";
	}

	public String alterar() {
		dao.atualizar(getTurma());
		this.turmas = (List<Turma>) (Object) dao.listar();
		return "gerenciarTurmas";
	}

	public String remover() {
		dao.remover(getTurma());
		this.turmas = (List<Turma>) (Object) dao.listar();
		return "gerenciarturmas";
	}
	
	public String enturmarAlunos(){
	
		int idade;
		MembroDAO membrosDao = new MembroDAO();
		List<Membro> membros = (List<Membro>) (Object) membrosDao.listar();
		List<Membro> membrosASeremEnturmados = new ArrayList<Membro>();
		
		for (Membro membro: membros){
			idade = membro.getIdade();
			if ((membro.getTipoMembro() == TipoMembro.ALUNO) && (idade < this.turma.getIdadeMaxima())
					&& (idade > this.turma.getIdadeMinima())){
				
				membrosASeremEnturmados.add(membro); 
			}
		}
		
		this.turma.setAlunos(membrosASeremEnturmados);		
		this.dao.atualizar(this.getTurma());
		return "gerenciarturmas";
	}
	
	public String verAlunosEnturmados (){
		
		return("alunosEnturmados");
	}

	public List<Turma> getTurmas() {
		return turmas;
	}

	public void setTurmas(List<Turma> turmas) {
		this.turmas = turmas;
	}

	public Turma getTurma() {
		this.turmas = (List<Turma>) (Object) dao.listar();
		return turma;
	}

	public boolean isCadastrada() {
		return cadastrada;
	}

	public void setCadastrada(boolean cadastrada) {
		this.cadastrada = cadastrada;
	}

	public BigDecimal getOferta() {
		return oferta;
	}

	public void setOferta(BigDecimal oferta) {
		this.oferta = oferta;
	}

	public void setTurma(Turma turma) {
		this.turma = turma;
	}

}
